paraview_add_plugin(MyFancyPlugin
  VERSION "1.0"
  MODULES MyFancyPluginFilters
  MODULE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/MyFancyPluginFilters/vtk.module")

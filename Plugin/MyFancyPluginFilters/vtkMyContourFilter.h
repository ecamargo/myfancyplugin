/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkMyContourFilter.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkMyContourFilter - generate 
// .SECTION Description
// vtkMyContourFilter is a filter to generate 

#ifndef vtkMyContouFilter_h
#define vtkMyContouFilter_h

#include "MyFancyPluginFiltersModule.h" // for export macro
#include "vtkFlyingEdges3D.h"

class MYFANCYPLUGINFILTERS_EXPORT vtkMyContourFilter : public vtkFlyingEdges3D
{
public:
  static vtkMyContourFilter* New();
  vtkTypeMacro(vtkMyContourFilter, vtkFlyingEdges3D);
  void PrintSelf(ostream& os, vtkIndent indent) override;

protected:
  vtkMyContourFilter();
  ~vtkMyContourFilter();

private:
  vtkMyContourFilter(const vtkMyContourFilter&) = delete;
  void operator=(const vtkMyContourFilter&) = delete;
};

#endif

Introduction
============

This is a plugin example for ParaView which is subclassing vtkFlyingEdges3D just to show it is possbile to have a VTK customized class available into ParaView 
This example was designed to leave room for adding more VTK modules.
ParaView is an open-source, multi-platform data analysis and
visualization application based on Visualization Toolkit (VTK).
There is a brief introduction to ParaView plugins in the folder presentation

Building
========

Before build the current plugin you need first build ParaView. This process can we found here https://www.paraview.org/Wiki/ParaView:Build_And_Install#Download_And_Install_CMake

Next you need create a build folder, run CMake and inform the ParaView_build_folder. Please refer to presentation folder for more details.


Plugin Loading
============

Copy MyFancyPlugin.dll and MyFancyPluginFiltersd.dll from <MyFancyPlugin_build>\bin\paraview-5.8\plugins\MyFancyPlugin\Debug\ into the same folder where Paraview.exe is.
Run ParaView and then use Plugin Manager to load our plugin.
#/bin/bash
rm -f *.out
rm -f *.nav
rm -f *.tps
rm -f *.aux
rm -f *.log
rm -f *.blg
rm -f *.bbl
rm -f *.snm
rm -f *.toc
rm -f *~
